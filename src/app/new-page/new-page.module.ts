import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewPageComponent } from './new-page.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [NewPageComponent],
  imports: [CommonModule, SharedModule],
})
export class NewPageModule { }
