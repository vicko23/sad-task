import { Component, OnInit } from '@angular/core';
import { SearchStore } from '../core/store/search-store';
import { Observable } from 'rxjs';
import { FilterOption, FILTER_OPTIONS_DATA } from 'src/app/shared/models/filterOption';
import { SearchState } from '../shared/models/searchState';

@Component({
  selector: 'app-new-page',
  templateUrl: './new-page.component.html',
  styleUrls: ['./new-page.component.css']
})
export class NewPageComponent implements OnInit {
  selectedFilterOption: FilterOption;
  searchStore$: Observable<SearchState>;

  constructor(private searchStore: SearchStore) { }

  ngOnInit(): void {
    this.searchStore$ = this.searchStore.state$;
    this.searchStore.emptyFilterOptions();
    FILTER_OPTIONS_DATA.forEach(fod => {
      this.searchStore.addFilterOption(fod);
    });
  }

}
