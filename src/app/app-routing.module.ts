import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoogleComponent } from './shared/components/search-engine/google/google.component';
import { BingComponent } from './shared/components/search-engine/bing/bing.component';
import { SearchComponent } from './search/search.component';
import { NewPageComponent } from './new-page/new-page.component';

const routes: Routes = [
  {
    path: 'search', component: SearchComponent,
    children: [
      { path: 'google', component: GoogleComponent },
      { path: 'bing', component: BingComponent },
    ]
  },
  {
    path: 'new-page', component: NewPageComponent,
  },
  { path: '',   redirectTo: '/search', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
