export class FilterOption {
  id: number;
  name: string;
}

export const FILTER_OPTIONS_DATA: FilterOption[] = [
  {
    id: 1,
    name: 'Category'
  },
  {
    id: 2,
    name: 'Date'
  },
  {
    id: 3,
    name: 'Description'
  }
];
