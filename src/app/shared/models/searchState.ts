import { FilterOption } from './filterOption';

export class SearchState {
  searchPhrase: string;
  filterOptions: FilterOption[];
}
