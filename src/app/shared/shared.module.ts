import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GoogleComponent } from './components/search-engine/google/google.component';
import { BingComponent } from './components/search-engine/bing/bing.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, RouterModule, HttpClientModule, FormsModule],
  exports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    GoogleComponent,
    BingComponent
  ],
  declarations: [GoogleComponent, BingComponent]
})
export class SharedModule {}
