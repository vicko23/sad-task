import { Component, OnInit } from '@angular/core';
import { SearchStore } from 'src/app/core/store/search-store';
import { SearchEngineComponent } from '../search-engine.component';

@Component({
  selector: 'app-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.css']
})
export class GoogleComponent extends SearchEngineComponent {
  searchEngineTitle = 'Google';

  constructor(searchStore: SearchStore) {
    super(searchStore);

    this.searchStore$.subscribe(s => {
      if (s && s.searchPhrase) {
        this.message = 'This is the ' + this.searchEngineTitle + ' component. You typed <b>' + s.searchPhrase + '</b>';
      }
    });
  }
}
