import { SearchStore } from '../../../core/store/search-store';
import { Observable } from 'rxjs';
import { SearchState } from '../../models/searchState';
import { OnDestroy, OnInit } from '@angular/core';

export class SearchEngineComponent implements OnDestroy, OnInit {
  protected searchEngineTitle: string;
  public message: string;
  protected searchStore$: Observable<SearchState>;

  constructor(private searchStore: SearchStore) {
    this.searchStore$ = this.searchStore.state$;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.searchStore.subscription.unsubscribe();
  }
}
