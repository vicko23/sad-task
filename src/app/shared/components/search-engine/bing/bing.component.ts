import { Component, OnInit } from '@angular/core';
import { SearchStore } from 'src/app/core/store/search-store';
import { SearchEngineComponent } from '../search-engine.component';

@Component({
  selector: 'app-bing',
  templateUrl: './bing.component.html',
  styleUrls: ['./bing.component.css']
})
export class BingComponent extends SearchEngineComponent {
  searchEngineTitle = 'Bing';

  constructor(searchStore: SearchStore) {
    super(searchStore);

    this.searchStore$.subscribe(s => {
      if (s && s.searchPhrase) {
        this.message = 'This is the ' + this.searchEngineTitle + ' component. You typed <b>' + s.searchPhrase + '</b>';
      }
    });
  }
}
