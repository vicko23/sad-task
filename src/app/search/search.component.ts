import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SearchStore } from '../core/store/search-store';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {
  searchPhrase: string;

  constructor(private router: Router, private searchStore: SearchStore) {
    this.searchStore.subscription = this.searchStore.state$.subscribe(sp => {
      this.searchPhrase = sp.searchPhrase;
    });
   }

  ngOnInit(): void {
  }

  selectSearchEngine(route: string) {
    this.router.navigateByUrl(route);
  }

  search(searchPhrase: string) {
    this.searchStore.setSearchPhrase(searchPhrase);
  }

  ngOnDestroy(): void {
    this.searchStore.subscription.unsubscribe();
  }
}
