import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { By, BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchStore } from '../core/store/search-store';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchComponent ],
      imports: [
        BrowserModule,
        SharedModule,
        RouterTestingModule
      ],
      providers: [SearchStore]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the search method', () => {
    fixture.detectChanges();
    spyOn(component, 'search');
    const searchInputEl: HTMLElement = fixture.debugElement.query(By.css('.form-control')).nativeElement;
    searchInputEl.dispatchEvent(new Event('input'));
    expect(component.search).toHaveBeenCalledTimes(1);
  });
});
