import { Injectable } from '@angular/core';
import { Store } from './root-store';
import { SearchState } from '../../shared/models/searchState';
import { FilterOption } from '../../shared/models/filterOption';

@Injectable()
export class SearchStore extends Store<SearchState> {
  constructor() {
    super(new SearchState());
    this.state.filterOptions = [];
  }

  setSearchPhrase(newSearchPhrase: string) {
    this.setState({
      ...this.state,
      searchPhrase: newSearchPhrase
    });
  }

  emptyFilterOptions() {
    this.setState({
      ...this.state,
      filterOptions: []
    });
  }

  addFilterOption(filterOption: FilterOption) {
    this.setState({
      ...this.state,
      filterOptions: [...this.state.filterOptions, filterOption]
    });
  }
}
