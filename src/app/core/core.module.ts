import { NgModule, Optional, SkipSelf } from '@angular/core';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { RouterModule } from '@angular/router';
import { SearchStore } from './store/search-store';

@NgModule({
  providers: [SearchStore],
  imports: [RouterModule],
  exports: [MainMenuComponent],
  declarations: [MainMenuComponent],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'Core module already loaded'
      );
    }
  }
}
