import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SearchModule } from './search/search.module';
import { NewPageModule } from './new-page/new-page.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    SearchModule,
    NewPageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
